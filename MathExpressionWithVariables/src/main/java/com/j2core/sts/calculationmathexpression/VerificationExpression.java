package com.j2core.sts.calculationmathexpression;

/**
 * Created by sts on 9/25/15.
 */
public class VerificationExpression {

    /**
     * The method is verification check correctness of the brackets in expression
     *
     * @param expression Expression for verification
     * @return  is brackets valid in expression
     */
    protected static boolean isBracketsValid (String expression){

        int bracketBalance = 0;

        for (int i = 0; i < expression.length(); i++) {
            if (expression.charAt(i) == '(') {
                bracketBalance++;
            }
            else if (expression.charAt(i) == ')') {
                bracketBalance--;
            }
            if(bracketBalance<0) return false;
        }
        return (bracketBalance == 0);
    }


    /**
     * The method is verification presence of extra points in expression
     *
     * @param expression Expression for verification
     * @return  are valid numbers in expression
     */
    protected static boolean isNumbersValid(String expression, String mathCharacters){

        int index = 0;
        while (index < expression.length()) {
            if (expression.charAt(index) == '-') {
                index++;
            }

            int point = 0;
            while (index < expression.length() && (Character.isDigit(expression.charAt(index)) || expression.charAt(index) == '.')) {

                if (expression.charAt(index) == '.' && ++point > 1) {
                    return false;
                }
                index++;
            }
            index++;
        }
        return true;

    }


    /**
     * The method is executing on the all verification for expression
     *
     * @param expression  Expression for verification
     * @return  valid expression or not
     * @throws VerificationDataException  if expression is not valid
     */
    public static boolean isExpressionValid (String expression, String mathCharacters) throws VerificationDataException {

        if (!isNumbersValid(expression, mathCharacters)) {
            throw new VerificationDataException(" This expression is not verification. The expression have not correct number(s). A number have two points");
        }
        if (!isBracketsValid(expression)){
            throw new VerificationDataException(" This expression is not verification. The expression have not correct brackets");
        }
        return true;
    }
}
