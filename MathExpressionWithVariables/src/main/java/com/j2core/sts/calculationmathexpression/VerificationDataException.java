package com.j2core.sts.calculationmathexpression;

/**
 * Created by sts on 10/14/15.
 */

/**
 *  The exception class for sand information if expression isn't valid
 */
public class VerificationDataException extends Exception {

    public VerificationDataException(String message){
        super(message);
    }

    public VerificationDataException(){};
}
