package com.j2core.sts.calculationmathexpression;

import java.util.List;

/**
 * Interface for calculation math actions
 */
public interface CalculationMathAction {

    double INACCURACY = 0.00000001;

    /**
     * The method calculated actions with numbers from Lists
     *
     * @param actions List with actions for calculated
     * @param numbers List with numbers for calculated
     * @return value all actions with numbers from Lists
     * @throws ArithmeticException if divisor in the division is zero
     */
    double calculationMathAction(List<Character> actions, List<Double> numbers);

}
