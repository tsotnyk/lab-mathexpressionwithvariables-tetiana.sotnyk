package com.j2core.sts.calculationmathexpression;

import java.util.List;

/**
 * The class is realization interface CalculationMathAction
 */
public class STSCalculationMathAction implements CalculationMathAction {

    char[] priorityActions;

    /**
     * Constructor for class STSCalculationMathAction
     *
     * @param priorityActions priority for math actions
     */
    public STSCalculationMathAction(char[] priorityActions) {
        this.priorityActions = priorityActions;
    }


    @Override
    public double calculationMathAction(List<Character> actionsExpression, List<Double> numbersExpression) throws ArithmeticException{

        int index;
        double result = 0;

        for (char action : priorityActions){
            index = actionsExpression.lastIndexOf(action);
            while (index > -1) {
                switch (action) {
                    case '^':
                        double degree = numbersExpression.get(index + 1);
                        result = involution(numbersExpression.get(index), (int) degree);
                        break;

                    case '/':
                        result = division(numbersExpression.get(index), numbersExpression.get(index + 1));
                        break;

                    case '*':
                        result = multiplying(numbersExpression.get(index), numbersExpression.get(index+1));
                        break;

                    case '+':
                        result = adding(numbersExpression.get(index), numbersExpression.get(index+1));
                        break;

                    case '-':
                        result = deduction(numbersExpression.get(index), numbersExpression.get(index+1));
                        break;
                }

                numbersExpression.remove(index+1);
                numbersExpression.set(index, result);
                actionsExpression.remove(index);
                index = actionsExpression.lastIndexOf(action);
            }
        }

        return numbersExpression.get(0);
    }


    /**
     * The method involution number on the degree
     *
     * @param number
     * @param degree
     * @return result involution
     */
    protected double involution(double number, int degree){

        double result = number;
        if (degree == 0){
            result = 1.0;
        }
        else {
            for (int i = 1; i < Math.abs(degree); i++){
                result = result * number;
            }
            if (degree < 0){
                result = 1 / result;
            }
        }

        return result;
    }


    /**
     * The method division numbers on the divisor
     *
     * @param number
     * @param divisor
     * @return result division
     */
    protected double division(double number, double divisor){

        if (divisor < Math.abs(INACCURACY)){
            throw new ArithmeticException(" Sorry! Division by zero is inadmissible.");
        }
        return number/divisor;
    }


    /**
     * The method multiplying first number with second numbers
     *
     * @param number1
     * @param number2
     * @return result multiplying
     */
    protected double multiplying (double number1, double number2){
        return number1*number2;
    }


    /**
     * The method calculated adding first number with second number
     *
     * @param number1
     * @param number2
     * @return result adding
     */
    protected double adding(double number1, double number2){
        return number1+number2;
    }


    /**
     * The method calculated deduction second number from first number
     *
     * @param number1
     * @param number2
     * @return result deduction
     */
    protected double deduction(double number1, double number2){
        return number1-number2;
    }

}
