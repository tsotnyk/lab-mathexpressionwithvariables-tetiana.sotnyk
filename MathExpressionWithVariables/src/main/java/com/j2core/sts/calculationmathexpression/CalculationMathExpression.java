package com.j2core.sts.calculationmathexpression;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Class for calculation expression with variables.
 */
public class CalculationMathExpression {

    CalculationMathAction calculationMathAction;
    String reservedSymbols = "()^/*-+";  // valid math actions
    final char OPENING_BRACKET = '(';
    final char CLOSING_BRACKET = ')';


    /**
     * Constructor for class CalculationMathExpression
     *
     * @param calculationMathAction  realization interface CalculationMathAction
     */
    public CalculationMathExpression(CalculationMathAction calculationMathAction) {
        this.calculationMathAction = calculationMathAction;
    }


    /**
     * The method compute result expression with specified value variables
     *
     * @param expression  expression for calculation
     * @param valueVariables  value for variables
     * @return  double value result for expression with specified value variables
     * @throws VerificationDataException if expression is not valid
     */
    public Double calculationExpression (String expression, double[] valueVariables) throws VerificationDataException {

        ArrayList<Double> numbersExpression = new ArrayList<>();
        ArrayList<Character> actionExpression = new ArrayList<>();

        expressionParse(expression, valueVariables, actionExpression, numbersExpression);

        return calculation(numbersExpression, actionExpression);

    }


    /**
     * The method calculated action with numbers from Lists
     *
     * @param numbersExpression  List numbers expression for calculation
     * @param actionExpression   List action expression for calculation
     * @return double result all action with numbers from Lists
     */
    protected Double calculation(List<Double> numbersExpression, List<Character> actionExpression){

        int indexFirstSymbol = actionExpression.indexOf(OPENING_BRACKET);
        while (indexFirstSymbol > -1){
            int indexSecondSymbol = actionExpression.indexOf(CLOSING_BRACKET);

            calculation(numbersExpression.subList(indexFirstSymbol, indexSecondSymbol), actionExpression.subList(indexFirstSymbol+1, indexSecondSymbol));

            actionExpression.remove(indexFirstSymbol+1);
            actionExpression.remove(indexFirstSymbol);

            indexFirstSymbol = actionExpression.indexOf(OPENING_BRACKET);
        }

        numbersExpression.set(0,calculationMathAction.calculationMathAction(actionExpression, numbersExpression));

        return numbersExpression.get(0);
    }


    /**
     * The method verification valid expression, parse expression and add data in the information Lists
     *
     * @param expression      expression for calculation
     * @param valueVariables  value for variables
     * @param actionExpression  List for saving action expression
     * @param numbersExpression List for saving numbers expression
     * @throws VerificationDataException  if not valid data (expression, numbers, or variables)
     */
    protected void expressionParse( String expression, double[] valueVariables, List<Character> actionExpression, List<Double> numbersExpression) throws VerificationDataException {

        boolean negative = false;
        int indexExpression = 0;
        int indexValueVariables = 0;
        double value;
        Map<String, Double> mapValueVariables = new TreeMap<>();

            VerificationExpression.isExpressionValid(expression, reservedSymbols);

            if (expression.contains(" ")) {
                expression = expression.replace(" ", "");
            }

            while (indexExpression < expression.length()) {

                if (expression.charAt(indexExpression) == OPENING_BRACKET || expression.charAt(indexExpression) == CLOSING_BRACKET){
                    actionExpression.add(expression.charAt(indexExpression));
                    indexExpression++;
                }

                if (expression.charAt(indexExpression) == '-') {
                    negative = true;
                    indexExpression++;
                }

                if (expression.charAt(indexExpression) == '+'){
                    indexExpression++;
                }

                // Parser value if first char is number
                if (Character.isDigit(expression.charAt(indexExpression)) || expression.charAt(indexExpression) == '.') {
                    int j = indexExpression;
                    while (indexExpression < expression.length() && (Character.isDigit(expression.charAt(indexExpression)) || expression.charAt(indexExpression) == '.')) {
                        indexExpression++;
                    }

                       if ((indexExpression - j) == 0) throw new VerificationDataException("Can't get valid number in '" + expression + "'");
                    value = Double.parseDouble(expression.substring(j, indexExpression));
                }

                // Set value for variable
                else {
                    int i = indexExpression;
                    while ( indexExpression < expression.length() && reservedSymbols.indexOf(expression.charAt(indexExpression)) < 0 ) {
                        indexExpression++;
                    }
                    if (!mapValueVariables.containsKey(expression.substring(i, indexExpression))){
                        if (indexValueVariables == valueVariables.length){
                            throw new VerificationDataException(" Insufficient value for variable! ");
                        }
                        mapValueVariables.put(expression.substring(i, indexExpression), valueVariables[indexValueVariables]);
                        indexValueVariables++;
                    }
                    value = mapValueVariables.get(expression.substring(i, indexExpression));
                }

                // if number is negative - set value to negative
                if (negative) {
                    value = -value;
                    negative = false;
                }

                numbersExpression.add(value);

                if (indexExpression < expression.length()){
                    if (expression.charAt(indexExpression) == CLOSING_BRACKET){
                        actionExpression.add(expression.charAt(indexExpression));
                        indexExpression++;
                    }
                    if (indexExpression < expression.length()){
                        actionExpression.add(expression.charAt(indexExpression));
                        indexExpression++;
                    }
                }
            }
    }
}
