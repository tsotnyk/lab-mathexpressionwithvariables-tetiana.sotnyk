package com.j2core.sts.calculationmathexpression;


import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * Created by sts on 10/6/15.
 */
public class TestNGCalculationMathExpression {

    private CalculationMathExpression calculation = new CalculationMathExpression(new STSCalculationMathAction(new char[] {'^', '/', '*', '-', '+'}));

    @DataProvider(name = "calculation")
    public Object[][] simpleDataProvider(){
        return  new Object[][]{
                {"-5+xyz- 45*sdfjuh34/4.2-(-yte+1*xyz)", new double[]{1, 2, 3}, -27.4286},
                {"7*(v^2-5)-7+v", new double[]{3}, 24},
                {"1-35+(p^2-r^1+d^0)", new double[]{3, 3, 3}, -27},
                {"1-35+(p^2-r^1+d^0)", new double[]{3, 3, 3}, -27}
        };
    }

    @Test(dataProvider = "calculation")
    public void testCalculationExpression(String expression, double[] value, double result) throws VerificationDataException {

            Assert.assertEquals(result, calculation.calculationExpression(expression, value), 0.001);
    }


}


