package com.j2core.sts.calculationmathexpression;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * Created by sts on 10/13/15.
 */
public class TestNgArithmeticExceptions {

    private CalculationMathExpression calculation = new CalculationMathExpression(new STSCalculationMathAction(new char[] {'^', '/', '*', '-', '+'}));

    @DataProvider(name = "calculation")
    public Object[][] simpleDataProvider(){

        return new Object[][]{
                {"tmp/0+34*56", new double[]{7}},
                {"45/(5^0-1)-34/i", new double[]{56}},
                {"variables-34/34-4/map", new double[] {3, 0}},
        };
    }

    @Test(dataProvider = "calculation", expectedExceptions = ArithmeticException.class)
    public void testNotExceptionsCalculationExpression(String expression, double[] value) throws VerificationDataException {

            calculation.calculationExpression(expression, value);

    }
}
