package com.j2core.sts.calculationmathexpression;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * Created by sts on 10/13/15.
 */
public class TestNGVerificationExpression {

    static String reservedSymbols = "()^/*-+";


    @DataProvider(name = "trueVerificationExpression")
    public Object[][] trueDataProvider(){
        return new Object[][]{
                {"5.43-.34*5^-2"},
                {"(5/2)+(5*2)"},
                {"-3-+3/(6--3)"}
        };
    }

    @Test(dataProvider = "trueVerificationExpression")
    public void testTrueVerificationExpression(String expression) throws VerificationDataException {

        VerificationExpression.isExpressionValid(expression, reservedSymbols);

    }

    @DataProvider(name = "falseVerificationExpression")
    public Object[][] falseDataProvider(){
        return new Object[][]{
                {".5.0-2.3/34"},
                {"(5-)+5)"},
                {"(5-3)+(6-4"}
        };
    }

    @Test(dataProvider = "falseVerificationExpression", expectedExceptions = VerificationDataException.class)
    public void testFalseVerificationExpression(String expression) throws VerificationDataException {

        VerificationExpression.isExpressionValid(expression, reservedSymbols);

    }
}
